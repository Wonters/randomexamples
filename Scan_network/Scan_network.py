# -*- coding: utf-8 -*-

import os
import subprocess
import re
import threading

regex = re.compile(r"(?P<received>\d+) received")

class Ping(threading.Thread):

    def __init__(self, hostname):
        threading.Thread.__init__(self)
        self.hostname = hostname

    def run(self):
        p = subprocess.Popen(["ping", "-c1", "-W100", self.hostname], stdout=subprocess.PIPE).stdout.read()
        r = regex.search(p.decode())
        try:
            print(r)
            if(r.group("received") == "1"):
                print("L'adresse %s existe!" % self.hostname) 
        except:
            pass

for i in range(254):
    hostname = "192.168.0.%i" % (i+1)
    background = Ping(hostname)
    background.start()

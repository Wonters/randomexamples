import cx_Freeze 
import sys 

base = None

if sys.platform == 'win32':
    base = "Win32GUI"

executables = [cx_Freeze.Executable("Scan_network.py",base=base)]

cx_Freeze.setup(
    name = "Scan_network",
    version = "1",
    description ="scan tho local network of the computeur with the ip 192.168.1.*",
    executables = executables
) 
    

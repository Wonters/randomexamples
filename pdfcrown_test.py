import pdfcrowd

import sys

try:
    # create the API client instance
    client = pdfcrowd.HtmlToPdfClient('demo', 'ce544b6ea52a5621fb9d55f8b542d14d')

    # create output stream for conversion result
    output_stream = open('MyLayout.pdf', 'wb')

    # run the conversion and write the result into the output stream
    client.convertFileToStream('./template.html', output_stream)

    # close the output stream
    output_stream.close()
except pdfcrowd.Error as why:
    # report the error
    sys.stderr.write('Pdfcrowd Error: {}\n'.format(why))

    # rethrow or handle the exception
    raise

from setuptools import setup

setup(
    name="how-click",
    version="1.0.0",
    py_modules=["click_example"],
    install_requirements=
    [
        'click',
    ],
    entry_points=
    """[console_scripts]
        how-click=click_example:cli
    """

)

import click 

@click.group()
def cli():
    pass

@cli.command()
@click.option('--world',)
@click.argument('name')
def hello_name(world, name):
    print(f"{world} {name}")



#! bin/python3

# lambda define a small anonymous function 
x = lambda a,b,c: a + b + c 

# It is more interesting when you use it in a function 


# Return a multiple of n
def multiple_function(n)
    return lambda multiple: multiple*n


double = multiple_function(2)
triple = multiple_function(3)

